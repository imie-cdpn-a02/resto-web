'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
  'ngRoute',
  'myApp.menu',
  'myApp.commande',
  'myApp.user',
  'myApp.view2',
  'myApp.version'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/connection'});
}])
.run(function($rootScope, $location) {

    // register listener to watch route changes
    $rootScope.$on("$routeChangeStart", function (event, next, current) {
        // Retrieve the user from storage
        var user = JSON.parse(localStorage.getItem('user'));
        if(user === null){
            $location.path( "/connection" );
        }
    });
});