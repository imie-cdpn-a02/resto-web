'use strict';

angular.module('myApp.commande', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/listCommande', {
            templateUrl: 'commande/views/listCommande.html',
            controller: 'listCommandeCtrl'
        });
    }])

    .controller('listCommandeCtrl', ['$scope', '$http', function ($scope, $http) {
        //Call the services
        $http.get('https://ftgo-order-api.herokuapp.com/api/orders').then(function (response) {
            if (response.data) {
                $scope.listCommandes = response.data;

                // Va chercher les infos de chaque commandes
                angular.forEach($scope.listCommandes, function(commande, key) {
                    console.log("Commande id : " + commande.id);

                    // Pour chaque menu de la commande
                    angular.forEach(commande.menu_orders, function(menu_order, key) {
                        console.log("Menu id : " + menu_order.ref_menu);
                        $http.get('https://ftgo-menu-api-staging.herokuapp.com/api_menu.php?idMenu=' + menu_order.ref_menu).then(function (response) {
                            if (response.data[0]) {
                                menu_order.menu = response.data[0];
                                console.log("menu " + menu_order.ref_menu + " : " + menu_order.menu.libelle);
                            }
                        });
                    });

                    // Client de la demande
                    console.log("Client id : " + commande.ref_client);
                    $http.get('https://ftgo-client-api-staging.herokuapp.com/clients/' + commande.ref_client).then(function (response) {
                        if (response.data) {
                            commande.client = response.data;
                        }
                    });
                });
            }
        }, function (response) {
            $scope.msg = "Service not Exists";
            $scope.statusval = response.status;
            $scope.statustext = response.statusText;
            $scope.headers = response.headers();
        });

    }]);