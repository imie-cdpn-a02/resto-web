'use strict';

angular.module('myApp.menu', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/newMenu', {
            templateUrl: 'menu/views/newMenu.html',
            controller: 'newMenuCtrl'
        });
        $routeProvider.when('/newRestaurant', {
            templateUrl: 'menu/views/newRestaurant.html',
            controller: 'newRestaurantCtrl'
        });
        $routeProvider.when('/listMenu', {
            templateUrl: 'menu/views/listMenu.html',
            controller: 'listMenuCtrl'
        });
    }])

    .controller('newMenuCtrl', [function ($scope, $http) {
        $scope.postdata = function (libelle, description, prix, duree, reference_restaurant) {
            var data = {
                libelle: libelle,
                description: description,
                prix: prix,
                duree: duree,
                reference_restaurant: reference_restaurant
            };
            //Call the services
            $http.post('/api/menu/create', JSON.stringify(data)).then(function (response) {
                if (response.data)
                    $scope.msg = "Post Data Submitted Successfully!";
            }, function (response) {
                $scope.msg = "Service not Exists";
                $scope.statusval = response.status;
                $scope.statustext = response.statusText;
                $scope.headers = response.headers();
            });
        }
    }])


    .controller('listMenuCtrl', ['$scope','$http', function ($scope, $http) {
        //Call the services
        $http.get('http://10.2.8.11/juillet2017/api_menu.php').then(function (response) {
            if (response.data){
                $scope.listMenu=response.data;
            }
        }, function (response) {
            $scope.msg = "Service not Exists";
            $scope.statusval = response.status;
            $scope.statustext = response.statusText;
            $scope.headers = response.headers();
        });
    }])

    .controller('newMenuCtrl', [function ($scope, $http) {
        $scope.postdata = function (libelle, description, prix, duree, reference_restaurant) {
            var data = {
                libelle: libelle,
                description: description,
                prix: prix,
                duree: duree,
                reference_restaurant: reference_restaurant
            };
            //Call the services
            $http.post('/api/users/post', JSON.stringify(data)).then(function (response) {
                if (response.data)
                    $scope.msg = "Post Data Submitted Successfully!";
            }, function (response) {
                $scope.msg = "Service not Exists";
                $scope.statusval = response.status;
                $scope.statustext = response.statusText;
                $scope.headers = response.headers();
            });
        }
    }]);