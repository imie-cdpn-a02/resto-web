'use strict';

angular.module('myApp.user', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/connection', {
    templateUrl: 'user/views/connection.html',
    controller: 'connectionCtrl'
  });
  $routeProvider.when('/deconnection', {
      templateUrl: 'user/views/connection.html',
      controller: 'deconnectionCtrl'
  });
  $routeProvider.when('/inscription', {
    templateUrl: 'user/views/inscription.html',
    controller: 'inscriptionCtrl'
  });
}])

.controller('connectionCtrl', [function($scope, $http ) {

    $scope.connection = function (email, password) {
        var data = {
            email: email,
            password: password
        };
        //Call the services
        $http.post('https://ftgo-client-api-staging.herokuapp.com/authenticate', JSON.stringify(data)).then(function (response) {
            if (response.data) {
                // Put the object into storage
                localStorage.setItem('user', JSON.stringify(response.data));
            }
        }, function (response) {
            $scope.msg = "Service not Exists";
            $scope.statusval = response.status;
            $scope.statustext = response.statusText;
            $scope.headers = response.headers();
        });
    }
}])

.controller('deconnectionCtrl', [function($scope, $rootScope) {
    localStorage.clear();
}])

.controller('inscriptionCtrl', [function($scope, $http) {

}]);